---
title: "Žebříček českých neověřených článků: dezinformační texty mají nad pravdivými navrch"
perex: "Všudypřítomné sociální sítě mění podobu médií. S tím, jak přibývá lidí, kteří čerpají informace především z Facebooku, se snižuje síla tradičních zpravodajských serverů a šíření dezinformací a fake news - úmyslně lživých článků - je čím dál jednodušší. Našli jsme stovku textů z dezinformačních webů, které se v Česku za poslední rok sdílely nejvíce."
description: "Všudypřítomné sociální sítě mění podobu médií. S tím, jak přibývá lidí, kteří čerpají informace především z Facebooku, se snižuje síla tradičních zpravodajských serverů a šíření dezinformací a fake news - úmyslně lživých článků - je čím dál jednodušší. Našli jsme stovku textů z dezinformačních webů, které se v Česku za poslední rok sdílely nejvíce."
authors: ["Michal Zlatkovský", "Petr Kočí"]
published: "24. listopadu 2016"
# coverimg: https://interaktivni.rozhlas.cz/fake-news/media/cover.png
# coverimg_note: "Foto <a href='#'>ČTK</a>"
socialimg: https://interaktivni.rozhlas.cz/dezinformace/media/social.png
url: "dezinformace"
libraries: [jquery, "https://interaktivni.rozhlas.cz/tools/datatables/1.10.12.min.js"]
styles: ["https://interaktivni.rozhlas.cz/tools/datatables/1.10.12.min.css"] 
recommended:
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/neznami-vlastnici-redaktori-a-nepruhledne-finance-prectete-kdo-v-cesku-siri-dezinformace--1673419
    title: Neznámí vlastníci, redaktoři a neprůhledné finance. Přečtěte si, kdo v Česku šíří dezinformace
    perex: Přiživují společenský neklid a strach, produkují nepřesné nebo nepravdivé informace. Na jedné straně demonizují Západ, na druhé chválí Východ. Dezinformační weby získávají na síle a éra sociálních sítí jim dodává čím dál vyšší popularitu. 
    image: http://media.rozhlas.cz/_obrazek/3751568--ukazka-titulku-clanku-parlamentnich-listu--1-479x353p0.png
  - link: https://interaktivni.rozhlas.cz/usvolby-explainer/
    title: 7 historických volebních momentů, které dovedly Ameriku k Trumpovi
    perex: Američané v úterý volí napříč celými Spojenými státy, souboj mezi Hillary Clintonovou a Donaldem Trumpem ale rozhodne jen hrstka z nich.
    image: https://interaktivni.rozhlas.cz/usvolby-explainer/media/7b196fec280a6ec5d81c5e33cfe9e01a/600x_.jpg
  - link: https://interaktivni.rozhlas.cz/brexit/
    title: Všechno, co jste chtěli vědět o brexitu
    perex: 23 otázek a odpovědí k britskému referendu o vystoupení z Evropské unie
    image: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
---

„Pokud je to pravda, nastane šok. Rozdělení ČSFR v roce 1992 je neplatné!“ Zpráva s tímto titulkem vyšla na internetu 4. března letošního roku a rychle si získala pozornost. Na Facebooku ji sdílely stránky <a target='_blank' href="https://www.facebook.com/ceskarealita/posts/951961951551381">Česká realita na tvrdo a bez cenzury</a> nebo <a target="_blank" href="https://www.facebook.com/cas.na.zmeny/posts/954357051319883">Svobodné Československo</a>, odkud se začala šířit mezi desetitísice lidí. Na sociální síti se na různých místech, třeba ve skupinách jako <a target='_blank' href="https://www.facebook.com/groups/pratele.ruska.v.cr/?fref=nf">Přátelé Ruska v České republice</a>, průběžně objevovala několik dalších měsíců - a dnes je s 26 tisíci lajky, sdíleními a komentáři mezi nejoblíbenějšími českými články na Facebooku za poslední rok.

Potíž je ovšem v tom, že - jak je patrné i z toho, že žádný šok nenastal - rozdělení  ČSFR rozhodně neplatné není. Text <a href="http://www.svetkolemnas.info/novinky/domaci/3959-pokud-je-to-pravda-nastane-sok-rozdeleni-csfr-v-roce-1992-je-zrejme-neplatne" rel="nofollow" target="_blank" class="potvrd">se odvolává</a> na skutečně schválený ústavní zákon, který v případě, že by Česko nebo Slovensko chtělo z federace vystoupit, nařizuje vypsat referendum. Jak je ovšem snadno dohledatelné, ČSFR nepřestala existovat vystoupením Česka nebo Slovenska z federace, ale <a target='_blank' href="https://www.zakonyprolidi.cz/cs/1992-542">zánikem federace jako takové</a>. 

## Svět fake news
Článek, který vyšel na webu Svět kolem nás, zabývajícím se v nepodepsaných článcích převážně konspiračními teoriemi, patří mezi takzvané fake news - tedy falešné, úmyslně zavádějící nebo přímo lživé zprávy. Fake news nejsou žádnou novinkou na internetu ani v tištěných médiích: například v Blesku vyšel těsně před prezidentskými volbami v roce 2013 <a target="_blank" href="http://zpravy.idnes.cz/pokuta-pro-pravnika-zavadila-za-inzerat-proti-schwarzenbergovi-p8y-/domaci.aspx?c=A131205_100522_zahranicni_skr">inzerát</a> namířený proti kandidátovi Karlu Schwarzenbergovi, za jehož neetičnost později dostal jeho zadavatel Vladimír Zavadil od České advokátní komory pokutu 850 tisíc. Důležitým tématem se ale v posledních týdnech falešné zprávy staly celosvětově.

Ve Spojených státech krátce před prezidentskou volbou zahltily Facebook a Twitter tisíce vymyšlených zpráv z nových, neznámých webových stránek. V drtivé většině podporovaly Donalda Trumpa: jedna o tom, jak mu vyjádřil důvěru papež, další třeba o připravované žalobě FBI na Hillary Clintonovou. Redaktoři webu BuzzFeed se rozhodli zjistit, jak lživé zprávy vznikají. <a target="_blank" href="https://www.buzzfeed.com/craigsilverman/how-macedonia-became-a-global-hub-for-pro-trump-misinfo">Prozkoumali přes stovku protrumpovských webů a došli k překvapivému zjištění.</a> Fake news má na svědomí skupinka makedonských teenagerů, kteří si tak vydělávají skrze zobrazování webové reklamy.

Nehledě na poněkud neotřelý původ falešných zpráv je ovšem jejich šíření skrz Google a Facebook závažným problémem a jak si všímají světová média, mohlo výsledek voleb přímo ovlivnit. BuzzFeed ukázal i to, že <a target="_blank" href="https://www.buzzfeed.com/craigsilverman/viral-fake-election-news-outperformed-real-news-on-facebook">20 nejsdílenějších falešných zpráv o volbách mělo na Facebooku větší odezvu než 20 nejsdílenějších zpráv skutečných</a>. Fake news sdílel i syn Donalda Trumpa Eric nebo manažer jeho volební kampaně Corey Lewandowski.

Na vlnu kritiky, která se zvedla po článcích BuzzFeedu a dalších zpravodajských webů, už zareagoval Google a později i Facebook, který podíl viny na šíření falešných zpráv zprvu odmítal. Obě stránky se zavázaly vyloučit tvůrce fake news ze svých reklamních systémů. Samotné odkazy na zdroje zpráv však stejně jako předtím nijak neblokují ani neomezují.

## Falešné zprávy v Česku: Nesmysly i dezinformace

<aside class="small">
<ul>
<li>K získání žebříčku článků jsme stejně jako BuzzFeed použili nástroj <a target="_blank" href="http://buzzsumo.com/">Buzzsumo</a>, kde jsme vyhledávali podle adres jednotlivých webů a s časovým rozmezím jednoho roku; počty sdílení uvedené v Buzzsumo jsme pak ověřili přímo přes rozhraní Facebooku.</li>
<li>Data jsou aktuální k 23. listopadu 2016; žebříček se neustále mění v závislosti na krocích Facebooku, který může u nahlášených odkazů čísla vynulovat.</li>
<li>Odkazy na dezinformační weby jsou opatřeny atributem _nofollow_, aby se nezvyšovala jejich pozice ve vyhledávačích; vstupujte na ně na vlastní nebezpečí.</li></aside>

Fake news jsou rozšířeným problémem také v Česku. Český rozhlas srovnal stejnou metodou jako BuzzFeed dvacet nejsdílenějších zpráv za poslední rok ze standardních zpravodajských serverů a z dezinformačních webů, jejichž seznam získal z <a target="_blank" href="http://www.evropskehodnoty.cz/fungovani-ceskych-dezinformacnich-webu/weby_list/think-tanku Evropské hodnoty">přehledu think tanku Evropské hodnoty</a> (ten svůj výběr odůvodňuje <a target="_blank" href="http://www.evropskehodnoty.cz/wp-content/uploads/2016/07/Fungov%C3%A1n%C3%AD-%C4%8Desk%C3%BDch-dezinforma%C4%8Dn%C3%ADch-web%C5%AF4.pdf">ve studii</a>). Výsledné zjištění je podobné jako v případě amerických voleb. Zatímco dvacet nejsdílenějších zpráv ze zpravodajských webů nasbíralo za rok zhruba 364,3 tisíce facebookových interakcí, u zpráv z dezinformačních webů to je 379,5 tisíce, tedy asi o čtyři procenta více.

Sto nejsdílenějších zpráv z dezinformačních webů za poslední rok shromáždil Český rozhlas do přehledné tabulky. Často přitom nejde o původní obsah daných stránek - ty přebírají texty jednak od sebe navzájem, jednak od zahraničních zdrojů fake news. Autoři jsou přitom prakticky vždy anonymní nebo používají pseudonymy; dezinformační weby také obvykle tají svou vlastnickou strukturu a způsob financování. Výjimkou jsou největší Parlamentní listy, které přes firmu Our Media <a target="_blank" href="https://www.mediaguru.cz/2015/04/valenta-jde-vyrazneji-do-medii-vstupuje-do-parlamentnich-listu/">spoluvlastní</a> senátor a majitel hazardní skupiny Synot Ivo Valenta.

<div data-bso=1></div>

<aside class="big"><table id="example" class="display"></table></aside>

Články z dezinformačních webů lze zhruba rozdělit do pěti kategorií. Ne všechny zprávy jsou přitom fake news, tedy úmyslně lživé informace.

### Zdravotní
Do kategorie falešných zdravotních zpráv patří konspirační teorie o škodlivosti očkování (<a href="http://www.rukojmi.cz/clanky/800-bill-gates-priznal-vakciny-maji-snizit-populaci" rel="nofollow" target="_blank" class="potvrd">Bill Gates přiznal: Vakcíny mají snížit populaci</a>), chemtrails (<a target='_blank' href="http://czechfreepress.cz/zdravi/chemtrailsova-chripka-uz-ji-mate.html" rel="nofollow" target="_blank" class="potvrd">Chemtrailsová chřipka: Už ji máte?</a>), mikrovln (<a href="http://www.lajkit.cz/zivotni-styl/item/374-z-tohoto-duvodu-rusko-v-roce-1976-zakazalo-mikrovlnne-trouby-informace-ktere-vam-zachrani-zdravi" rel="nofollow" target="_blank" class="potvrd">Z tohoto důvodu Rusko v roce 1976 zakázalo mikrovlnné trouby. Informace, které vám zachrání zdraví</a> - mimochodem <a target="_blank" href="https://www.quora.com/Are-microwave-ovens-illegal-in-certain-countries-Are-they-actually-harmful">nezakázalo</a> či o zázračném způsobu vyléčení rakoviny, který farmaceutickým průmyslem uplacení vědci tají (vůbec nejsdílenější <a href="http://www.bezpolitickekorektnosti.cz/?p=61541" rel="nofollow" target="_blank" class="potvrd">Ruský profesor: Cukrovka a rakovina jsou vymyšlené nemoci. Všechno je ve stravě. Peroxid vodíku – všelék? Věřit pohádkám, nebo zůstat v pasti</a>).

### Uprchlická krize
Patří sem články týkající se islámu, islámského terorismu a migrantů z Asie a Afriky. Na "rakouský deník Direkt" (ve skutečnosti další z mnoha internetových zdrojů fake news) se například odvolává článek <a href="http://ceskoaktualne.cz/2016/01/zpravy-ze-sveta/uprchlicka-krize-aneb-kdo-stoji-za-touto-imigracni-vlnou/" rel="nofollow" target="_blank" class="potvrd">Uprchlická krize aneb kdo stojí za touto imigrační vlnou?</a>; viníkem jsou prý USA (skrz financování převaděčů uprchlíků) a jejich "snaha oslabit Evropu a její integraci". Přeložená falešná zpráva je také například <a href="http://www.svetkolemnas.info/novinky/zahranicni/3559-do-nemecka-jsou-svazeny-autobusy-plne-ceskych-prositutek-pro-uspokojeni-mladych-utecencu" rel="nofollow" target="_blank" class="potvrd">Do Německa jsou sváženy autobusy plné českých prostitutek, pro uspokojení mladých utečenců</a>.

### Celebrity
Jde o oblíbený žánr zejména Parlamentních listů. Rozhovorem s celebritou, případně přepsáním jejího vyjádření pro jiné médium, lze dodat neověřitelným, nepravdivým či přímo záměrně lživým výrokům konkrétní zdroj a publikovat je s odvoláním na to, že jde přece o citaci cizího názoru. Ta nejskandálnější vyjádření je vhodné uvést přímo v titulku. Příkladem je text <a href="http://www.parlamentnilisty.cz/arena/monitor/Karel-Gott-Uprchlici-strkaji-pred-kameru-deti-za-nimi-jdou-mladi-muzi-v-modernich-ohozech-Amerika-nas-chce-drzet-ve-strachu-abychom-nerostli-Iluminati-nedovoli-Trumpa-429151" rel="nofollow" target="_blank" class="potvrd">Karel Gott: Uprchlíci strkají před kameru děti, za nimi jdou mladí muži v moderních ohozech. Amerika nás chce držet ve strachu, abychom nerostli. Ilumináti nedovolí Trumpa</a>.

### Rusko a Vladimir Putin
Odpor proti migraci a šíření konspiračních teorií je u českých dezinformačních webů spojeno s adorací Ruska a jeho prezidenta Vladimira Putina. Jde o zprávy s reálným základem, ale i naprosté výmysly; často jsou převzaty ze zahraničních stránek, kde lze ale jako zdroj dohledat Kremlem ovládanou zpravodajskou agenturu Sputnik, kterou například magazín Foreign Policy označuje za <a target="_blank" href="http://foreignpolicy.com/2014/11/10/kremlins-sputnik-newswire-is-the-buzzfeed-of-propaganda/">zdatného šiřitele ruské propagandy</a>. Typickými příklady jsou <a href="http://www.lajkit.cz/zpravy/item/687-putin-rusko-zakaz-rothschild" rel="nofollow" target="_blank" class="potvrd">Odvážný krok. Putin zakázal vstup do Ruska Rothschildům a bankovnímu kartelu NWO</a> nebo <a href="http://svobodnenoviny.eu/clovek-musi-byt-slepy-nebo-uplny-hlupak-aby-si-neuvedomil-ze-rusko-v-syrii-brani-evropu/" rel="nofollow" target="_blank" class="potvrd">Člověk musí být slepý, nebo úplný hlupák, aby si neuvědomil, že Rusko v Sýrii brání Evropu</a>.

### Ostatní
Řada zpráv z žebříčku patří do nezařaditelné směsky konspiračních teorií, alarmistických zpráv, zavádějících titulků a inspirativní moudrosti. Patří mezi ně úvodní <a href="http://www.svetkolemnas.info/novinky/domaci/3959-pokud-je-to-pravda-nastane-sok-rozdeleni-csfr-v-roce-1992-je-zrejme-neplatne" rel="nofollow" target="_blank" class="potvrd">Pokud je to pravda, nastane šok. Rozdělení ČSFR v roce 1992 je zřejmě neplatné!</a> nebo "inspirativní" <a href="http://www.svetkolemnas.info/novinky/zajimavosti/3777-12-citatu-z-maleho-prince-kterym-jste-jako-deti-nerozumeli-ale-nyni-vam-mohou-zmenit-zivot" rel="nofollow" target="_blank" class="potvrd">12 citátů z Malého prince, kterým jste jako děti nerozuměli, ale nyní vám mohou změnit život</a>. (Zda mohou citáty někomu změnit život, ověřit nelze, jisté ale je, že přinejmenším některé z nich z Malého prince vůbec nejsou.)


<!--Všechno to má ale společné něco, co nám řekne Jakub Macek. (nedůvěra v tradiční média)-->

V Česku se bojem s fake news zabývá hned několik webů a skupin. Vyvracením "tradičních" konspirací a mýtů se věnuje stránka <a target="_blank" href="http://hoax.cz">hoax.cz</a>. Ruské dezinformační kampani se <a target="_blank" href="http://www.evropskehodnoty.cz/fungovani-ceskych-dezinformacnich-webu/fungovani-ceskych-dezinformacnich-webu-2/">věnuje</a> zmíněný think-tank Evropské hodnoty. Pozornost si také získal projekt studentů Masarykovy univerzity <a target="_blank" href="http://zvolsi.info">zvolsi.info</a>, který si klade za cíl zvýšit povědomí o mediální gramotnosti mezi mladými lidmi.